<?php
    require_once "CityRestaurant.php";
    require_once "SultansDine.php";


    $CityRestaurant = new CityRestaurant("City Restaurant", "Sector 12, Uttara");

    echo "<hr>";
    
    echo " <b>{$CityRestaurant->orderFood("Naan")}</b> <br><br>";
    
    echo " <b>{$CityRestaurant->orderFood("Chicken Grill")}</b> <br><br>";
    
    echo "<b>{$CityRestaurant->calculateBill()}</b><br><br>";

    echo "<b>{$CityRestaurant->addItemToMenu("Sheek Kabab", 100)}</b></b>";

    echo "<hr><br>";



    $SultansDine = new SultansDine("Sultan's Dine", "Sector 11, Uttara");

    echo "<hr>";
    
    echo " <b>{$SultansDine->orderFood("Kacchi")}</b> <br><br>";
    
    echo "<b>{$SultansDine->addItemToMenu("Firni", 70)}</b><br><br>";

    echo " <b>{$SultansDine->orderFood("Firni")}</b> <br><br>";

    echo "<b>{$SultansDine->calculateBill()}</b><br><br>";

    echo "<hr>";


?>