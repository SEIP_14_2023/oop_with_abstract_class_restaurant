<?php
    require_once "Restaurant.php";

    class SultansDine extends Restaurant // Sultan's Dine
    {
        public function __construct($name,$location)
        {
            $this->name = $name;
            $this->location = $location;
            $this->totlPrice = 0;
            echo "<h1 align='center'> {$this->welcomeMessage()} </h1> <br>";
            $this->addItemToMenu();
        }
        public function addItemToMenu($food="",$price=0)
        {
            if($food=="")
            {
                // Menu Item
                $this->menu = array(
                    "Mutton Tehari" => 230,
                    "Kacchi" => 299,
                    "Chicken Roase" => 150
                );

                echo "Our Menu..<br>";
                foreach($this->menu as $key=>$value)
                {
                    echo "{$key} : {$value} <br>";
                }
                echo "<br>";
            }
            else
            {
                // add new item
                $this->menu[$food] = $price;
                echo "New item added successfully. <br>";
                echo "Our New Menu..<br>";
                foreach($this->menu as $key=>$value)
                {
                    echo "{$key} : {$value} <br>";
                }
                echo "<br>";
            }
        }        
    }

?>