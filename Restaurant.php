<?php
abstract class Restaurant {
    protected $name;
    protected $location;
    protected $menu;
    protected $totlPrice;

    public function welcomeMessage()
    {
        return "Welcome to {$this->name} located in {$this->location}.<br>Enjoy your meal!";
    }

    public function orderFood($food)
    {
        $this->totlPrice += $this->menu[$food];
        return "Food Name : {$food}, Price : {$this->menu[$food]} tk. Your order has been confirmed. Please wait some time!";
    }
    public function calculateBill()
    {
        return "Your total bill is {$this->totlPrice} tk.";
    }
    abstract public function addItemToMenu($food,$price);
}

?>